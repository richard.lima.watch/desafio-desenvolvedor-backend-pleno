# Desafio - Desenvolvedor Backend Pleno

## Objetivo:

Desenvolver a lógica de backend para uma plataforma de streaming de vídeo, desde a gestão de conteúdo até a entrega eficiente de vídeos aos usuários, utilizando tecnologias como Node.js ou PHP, e permitindo a escolha da base de dados.

## Requisitos:

## Gestão de Conteúdo:

- Criar endpoints de API para CRUD (Create, Read, Update, Delete) de vídeos.
- Implementar endpoints para categorização e etiquetagem de vídeos, permitindo uma organização eficiente do conteúdo.
- Garantir a integridade dos dados, validando e sanitizando todas as entradas relacionadas aos vídeos.


## Streaming de Vídeos:

- Desenvolver endpoints de API para fornecer streaming de vídeos, permitindo aos usuários assistir ao conteúdo de forma eficiente.
- Implementar uma estratégia de entrega de conteúdo adaptável (adaptive bitrate streaming), garantindo uma experiência de visualização suave em diferentes velocidades de conexão.
- Integrar medidas de segurança para prevenir a pirataria e proteger os direitos autorais dos vídeos.


## Segurança:

- Implementar autenticação e autorização de usuários, permitindo que apenas usuários autenticados possam acessar o conteúdo da plataforma.
- Proteger as rotas sensíveis com middleware de autenticação, garantindo que apenas usuários autorizados possam assistir aos vídeos.

## Desempenho:

- Otimizar a entrega de vídeos utilizando técnicas como cache de conteúdo e compressão de dados, garantindo uma experiência de streaming rápida e confiável.
- Implementar um sistema de monitoramento para acompanhar o desempenho do serviço de streaming, identificar gargalos e otimizar a infraestrutura conforme necessário.


## Logging e Monitoramento:

- Configurar logging para registrar eventos importantes, como acesso aos vídeos, erros de reprodução e tentativas de acesso não autorizado.
- Integrar ferramentas de monitoramento para acompanhar o desempenho da aplicação, identificar problemas de escalabilidade e garantir a disponibilidade do serviço.


## Testes:

- Implementar testes automatizados para garantir a robustez e confiabilidade da aplicação, cobrindo unidades de código, integração de componentes e casos de uso de streaming de vídeo.

## Diferenciais

- Publicação no Vercel.app
- Uso de Containers Docker
- Build para produção

## Dicas:

- Utilize o padrão de projeto MVC (Model-View-Controller) para organizar o código de forma modular e escalável.
- Faça uso de bibliotecas e frameworks populares de sua preferência para o Node.js ou PHP, e bibliotecas de streaming de vídeo como Video.js ou HLS.js.
- Mantenha o código limpo, bem documentado e siga as melhores práticas de desenvolvimento de software.
- Desenvolva a solução de acordo com os requisitos especificados, garantindo um código de alta qualidade e uma experiência de streaming de vídeo fluida para os usuários da plataforma.

## Como Contribuir

- Faça um fork deste repositório.
- Crie uma branch com a sua feature: git checkout -b feature/nova-feature
- Faça commit das suas mudanças: git commit -m 'Adiciona nova feature'
- Faça push para a sua branch: git push origin feature/nova-feature
- Faça um pull request neste repositório.

# Boa Sorte!!
